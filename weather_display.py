#! /usr/bin/python3

import os
import json
import logging
import sched
import time
import signal
import sys
from RPi import GPIO
from threading import Thread, Event
from lib.ban import BaseAdressesNationale
from lib.weather_data import WeatherData
from lib.lcd_display import LcdDisplay

# Logging configuration
logging.basicConfig()
log = logging.getLogger('main')
log.setLevel(logging.INFO)

# Global button thread
button_thread = None


def read_config_file(config_file):
    """Read JSON configuration file and load the configuration.

    Parameter:
        config_file: Path to the configuration file.
    """
    # Open and read configuration file
    with open(config_file, 'r+') as json_config:
        config = json.load(json_config)

        # Sanity check
        if 'location' not in config or 'address' not in config['location']:
            raise KeyError('No address provided')

        # Retrieve citycode, latitude, and longitude if not provided
        if 'citycode' not in config['location'] or\
           'latitude' not in config['location'] or\
           'longitude' not in config['location']:
            ban_data = BaseAdressesNationale(config['location']['address'])
            config['location']['citycode'] = ban_data.get_citycode()
            config['location']['longitude'] = ban_data.get_coordinates()[0]
            config['location']['latitude'] = ban_data.get_coordinates()[1]

            # Store in configuration file
            json_config.seek(0)
            json_config.truncate()
            json.dump(config, json_config, indent=4)

        # Close configuration file
        json_config.close()

        return config


def button_task(display):
    """Task in charge of the button press.

    Parameter:
        display: Display object used to display data on screen.
    """
    global button_thread

    # Button handler: pin 4 and 3 seconds for long press
    button_bcm_pin = 4
    button_long_press = 3 / 1000
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(button_bcm_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    while True:
        if button_thread._stop_event.is_set():
            log.debug('Request to stop button thread')
            sys.exit(0)
        if GPIO.wait_for_edge(button_bcm_pin,
                              GPIO.FALLING,
                              timeout=5000) is not None:
            push_start = time.process_time()
            duration = 0
            while not GPIO.input(button_bcm_pin):
                duration = time.process_time() - push_start
                if duration > button_long_press:
                    break
                time.sleep(0.1)
            if duration > button_long_press:
                log.debug('Button long press')
                display.lcd_init()
                display.show_current_page()
            else:
                log.debug('Button was pressed')
                display.show_next_page()


def timeout_handler(signum, frame):
    """Data retrieving timeout handler."""
    global button_thread

    log.error('Timeout while retrieving data')

    # Request to stop button thread
    button_thread._stop_event.set()
    # Wait for thread end
    button_thread.join()
    # Bye!
    sys.exit(1)


def retrieve_data(sc, weather_data, display):
    """Weather data retriever.

    Parameters:
        sc: Python Scheduler.
        weather_data: WeatherData object where retrieved data is stored.
        display: Display object used to display data on screen.
    """
    # Register an alarm to timeout after one minute
    signal.signal(signal.SIGALRM, timeout_handler)
    signal.alarm(60)

    # Retrieve rain forecast
    weather_data.retrieve_rain()

    # Retrieve Netatmo temperatures
    weather_data.retrieve_temperatures_and_co2()

    # Retrieve Dark Sky daily forecast
    weather_data.retrieve_forecast()

    # Success, disable alarm
    signal.alarm(0)

    # Update display
    display.show_current_page()

    # Reschedule data retrieval, every 5 minutes
    sc.enter(5 * 60, 1, retrieve_data, (sc, weather_data, display))


def main():
    """Main function."""
    global button_thread

    # Disable GPIO warnings
    GPIO.setwarnings(False)

    # Read configuration from file
    config = read_config_file(
                    os.path.join(
                        os.path.dirname(os.path.realpath(__file__)),
                        'config',
                        'config.json'
                    )
                )

    # Create WeatherData object
    weather_data = WeatherData(config)
    # Create Display object
    display = LcdDisplay(weather_data)

    # Start button thread
    button_thread = Thread(target=button_task, args=(display, ))
    button_thread._stop_event = Event()
    button_thread.start()

    # Schedule first data retrieval, as soon as possible
    s = sched.scheduler(time.time, time.sleep)
    s.enter(0, 1, retrieve_data, (s, weather_data, display))
    # Blocking call!
    s.run()


if __name__ == '__main__':
    main()
