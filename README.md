# Weather display
Display weather data on a 16*2 LCD screen.

## Installation
In order to fetch the necessary Python packages, just run `pip3 install -r requirements.txt`.

## Configuration
The configuration file is in the `config` folder, and should be named `config.json`.

An example is provided with `config_example.json`: rename it as `config.json` and replace the dummy parameters with:
* Your address.
* Your API credentials from [Netatmo](http://dev.netatmo.com/doc/).
* Your API key from [Dark Sky](https://darksky.net/dev).

## Wiring
Wired pins are hard-coded as the following Raspberry Pi [BCM pins](https://pinout.xyz/#):
* A0 (button) -> BCM 4
* D4 -> BCM 22
* D5 -> BCM 23
* D6 -> BCM 24
* D7 -> BCM 25
* D8 -> BCM 17
* D9 -> BCM 18

## Usage
Make sure the `FR_fr` locale is available. If not, enable it using `sudo dpkg-reconfigure locales`.

Just run the `weather_display.py` script!

A short press on the Up or Right button allows to switch the displayed page.

A long press on the Up or Right button allows to reset the LCD display in case it behaves strangely.

## systemd service
A `weather_display.service` service file is available. In order to enable it:
```
sudo cp weather_display.service /etc/systemd/system/weather_display.service
sudo systemctl enable weather_display.service
sudo systemctl start weather_display.service
sudo systemctl status weather_display.service
```

## Documentation
Here are some useful links.
### LCD display
This script uses a DF Robot LCD screen:
* http://www.shieldlist.org/dfrobot/lcd
* https://wiki.dfrobot.com/Arduino_LCD_KeyPad_Shield__SKU__DFR0009_

### Raspberry Pi pinout
Help in order to find the BCM pins:
* https://www.raspberrypi.org/documentation/usage/gpio/
* https://pinout.xyz/#
