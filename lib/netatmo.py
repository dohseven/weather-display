#! /usr/bin/python3

import lnetatmo
import logging

# Logging configuration
logging.basicConfig()
log = logging.getLogger('netatmo')
log.setLevel(logging.INFO)


class Netatmo:
    """Connect to your Netatmo account in order to retrieve
    the data from your weather station.

    Parameters are the credentials retrieved from https://dev.netatmo.com:
        client_id
        client_secret
        refresh_token
    """

    def __init__(self, client_id, client_secret, refresh_token):
        """Netatmo initializer."""
        # Connect to Netatmo server
        auth = lnetatmo.ClientAuth(client_id, client_secret, refresh_token)
        self.dev_list = lnetatmo.WeatherStationData(auth)

    def get_temperatures_and_co2(self):
        """Retrieve temperatures and CO2 level from your weather station.

        Returns a list of modules with temperatures and CO2 level,
        None on failure.
        """
        if not self.dev_list:
            return None

        data = {}

        # Retrieve data
        weather_data = self.dev_list.lastData()

        for module, module_data in weather_data.items():
            log.debug(
                f'{module}: '
                f'{module_data["Temperature"]}°C'
            )
            data[module] = {}
            data[module]['temperature'] = module_data['Temperature']
            if 'CO2' in module_data:
                data[module]['co2'] = module_data['CO2']
                log.debug(f'{module_data["CO2"]} PPM')

        return data
