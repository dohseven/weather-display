#! /usr/bin/python3

from darksky.api import DarkSky
from darksky.types import languages, units, weather
from darksky.exceptions import DarkSkyException
import logging

# Logging configuration
logging.basicConfig()
log = logging.getLogger('DarkSky')
log.setLevel(logging.INFO)


class DarkSkyWeather:
    """Retrieve weather forecast from Dark Sky for the given location.

    Parameters:
        api_key: Dark Sky API key.
        coordinates: Coordinates [latitude, longitude] of the location.
    """

    def __init__(self, api_key, latitude, longitude):
        """DarkSkyWeather initializer."""
        self.api_key = api_key
        self.latitude = latitude
        self.longitude = longitude

    def get_daily_forecast(self):
        """Retrieve weather forecast."""
        dark_sky = DarkSky(self.api_key)
        if self.latitude and self.longitude:
            try:
                forecast = dark_sky.get_forecast(
                    self.latitude,
                    self.longitude,
                    extend=False,
                    lang=languages.FRENCH,
                    values_units=units.AUTO,
                    exclude=[weather.MINUTELY, weather.ALERTS],
                    timezone=None
                )
                return forecast.daily
            except DarkSkyException as err:
                log.error(f'Failed to retrieve Dark Sky data: {err}')
                return None
        return None
