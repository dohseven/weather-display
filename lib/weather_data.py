#! /usr/bin/python3

import logging
from threading import Lock
from lib.meteo_france import MeteoFranceRainForecast
from lib.netatmo import Netatmo
from lib.darksky_weather import DarkSkyWeather

# Logging configuration
logging.basicConfig()
log = logging.getLogger('WeatherData')
log.setLevel(logging.INFO)


class WeatherData:
    """Store weather data:
    - Rain forecast for the next hour, retrieved from Météo France.
    - Current temperatures and co2 levels, retrieved from Netatmo.
    - Weather forecast, retrieved from Dark Sky.

    Parameter:
        config: JSON configuration.
    """

    def __init__(self, config):
        """WeatherData initializer."""
        self.config = config
        self.mutex = Lock()
        self._rain = None
        self._temperatures_and_co2 = None
        self._forecast = None

    @property
    def rain(self):
        """Get the rain forecast for the next hour."""
        self.mutex.acquire()
        rain = self._rain
        self.mutex.release()
        return rain

    @rain.setter
    def rain(self, r):
        """Set the rain forecast for the next hour."""
        self.mutex.acquire()
        self._rain = r
        self.mutex.release()

    @property
    def temperatures_and_co2(self):
        """Get the current temperatures and co2 levels."""
        self.mutex.acquire()
        temperatures_and_co2 = self._temperatures_and_co2
        self.mutex.release()
        return temperatures_and_co2

    @temperatures_and_co2.setter
    def temperatures_and_co2(self, t_and_c):
        """Set the current temperatures and co2 levels."""
        self.mutex.acquire()
        self._temperatures_and_co2 = t_and_c
        self.mutex.release()

    @property
    def forecast(self):
        """Get the weather forecast."""
        self.mutex.acquire()
        forecast = self._forecast
        self.mutex.release()
        return forecast

    @forecast.setter
    def forecast(self, f):
        """Set the weather forecast."""
        self.mutex.acquire()
        self._forecast = f
        self.mutex.release()

    def retrieve_rain(self):
        """Retrieve rain forecast for the next hour."""
        if 'citycode' in self.config['location']:
            rain = MeteoFranceRainForecast(
                        self.config['location']['latitude'],
                        self.config['location']['longitude']
                    ).get_rain_forecast()
            log.debug(rain)
            self.rain = rain

    def retrieve_temperatures_and_co2(self):
        """Retrieve Netatmo temperatures and CO2 levels."""
        if 'netatmo' in self.config:
            temperatures_and_co2 = Netatmo(
                                self.config['netatmo']['client_id'],
                                self.config['netatmo']['client_secret'],
                                self.config['netatmo']['refresh_token']
                            ).get_temperatures_and_co2()
            log.debug(temperatures_and_co2)
            self.temperatures_and_co2 = temperatures_and_co2

    def retrieve_forecast(self):
        """Retrieve Dark Sky forecast."""
        if 'dark_sky' in self.config:
            forecast = DarkSkyWeather(
                            self.config['dark_sky']['api_key'],
                            self.config['location']['latitude'],
                            self.config['location']['longitude']
                        ).get_daily_forecast()
            if forecast:
                for day in forecast:
                    log.debug(day.time.strftime('%a %d/%m'))
                    log.debug(day.summary)
            self.forecast = forecast
