#! /usr/bin/python3

import requests
import logging

# Logging configuration
logging.basicConfig()
log = logging.getLogger('BAN')
log.setLevel(logging.INFO)


class BaseAdressesNationale:
    """Retrieve various geographical data for a given address.

    Parameter:
        Address to search.
    """

    def __init__(self, address):
        """BaseAdressesNationale initializer."""
        self.address = address
        try:
            r = requests.get('https://api-adresse.data.gouv.fr/search/',
                             params={'q': address, 'limit': 1})
            r.raise_for_status()
            # See documentation: https://geo.api.gouv.fr/adresse
            self.geojson = r.json()['features'][0]
        except requests.exceptions.HTTPError as err:
            log.error(f'Failed to retrieve GeoJSON: {err}')
            self.geojson = None

    def get_citycode(self):
        """Retrieve citycode for the given address."""
        if not self.geojson:
            return None

        citycode = self.geojson['properties']['citycode']
        log.debug(f'Found citycode {citycode} for address {self.address}')
        return citycode

    def get_coordinates(self):
        """Retrieve geographical coordinates for the given address."""
        if not self.geojson:
            return [None, None]

        coordinates = self.geojson['geometry']['coordinates']
        log.debug(f'Found coordinates {coordinates} '
                  f'for address {self.address}')
        return coordinates
