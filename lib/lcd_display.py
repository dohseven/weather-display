#! /usr/bin/python3

import logging
import locale
from RPi import GPIO
from RPLCD.gpio import CharLCD
from datetime import timedelta

# Logging configuration
logging.basicConfig()
log = logging.getLogger('LCD display')
log.setLevel(logging.INFO)


class LcdDisplay:
    def __init__(self, weather_data):
        # Set locale for days display
        locale.setlocale(locale.LC_ALL, 'fr_FR')
        # Display pages
        self.pages = {
            0: self.__show_rain_forecast,
            1: self.__show_netatmo_temperatures_and_co2,
            2: self.__show_forecast,
            3: self.__show_forecast,
            4: self.__show_forecast,
            5: self.__show_forecast,
            6: self.__show_forecast,
            7: self.__show_forecast,
            8: self.__show_forecast,
        }
        self.current_page = 0
        # Custom LCD characters
        self.custom_lcd_chars = {
            'rain_1': 0,
            'rain_2': 1,
            'rain_3': 2,
            'rain_4': 3,
            'degree': 4,
        }
        # Map rain level to custom character
        self.rain_level_map = {
            0: self.custom_lcd_chars['rain_1'],
            1: self.custom_lcd_chars['rain_1'],
            2: self.custom_lcd_chars['rain_2'],
            3: self.custom_lcd_chars['rain_3'],
            4: self.custom_lcd_chars['rain_4'],
        }
        # Store weather data
        self.weather_data = weather_data
        # Initialize LCD
        self.lcd_init()

    def lcd_init(self):
        """Initialize LCD display."""
        self.lcd = CharLCD(pin_rs=17, pin_e=18,
                           pins_data=[22, 23, 24, 25],
                           numbering_mode=GPIO.BCM,
                           cols=16, rows=2,
                           charmap='A02')
        custom_char = (0b00000,
                       0b00000,
                       0b00000,
                       0b00000,
                       0b00000,
                       0b00000,
                       0b00000,
                       0b11111)
        self.lcd.create_char(self.custom_lcd_chars['rain_1'], custom_char)
        custom_char = (0b00000,
                       0b00000,
                       0b00000,
                       0b00000,
                       0b11111,
                       0b11111,
                       0b11111,
                       0b11111)
        self.lcd.create_char(self.custom_lcd_chars['rain_2'], custom_char)
        custom_char = (0b00000,
                       0b00000,
                       0b11111,
                       0b11111,
                       0b11111,
                       0b11111,
                       0b11111,
                       0b11111)
        self.lcd.create_char(self.custom_lcd_chars['rain_3'], custom_char)
        custom_char = (0b11111,
                       0b11111,
                       0b11111,
                       0b11111,
                       0b11111,
                       0b11111,
                       0b11111,
                       0b11111)
        self.lcd.create_char(self.custom_lcd_chars['rain_4'], custom_char)
        custom_char = (0b00110,
                       0b01001,
                       0b01001,
                       0b00110,
                       0b00000,
                       0b00000,
                       0b00000,
                       0b00000)
        self.lcd.create_char(self.custom_lcd_chars['degree'], custom_char)
        self.lcd.clear()

    def __show_string(self, string):
        log.debug(string)
        self.lcd.clear()
        self.lcd.write_string(string)

    def __show_rain_forecast(self):
        if not self.weather_data.rain:
            self.__show_string('No rain data')
            return

        # Line 1: start and end time
        start = self.weather_data.rain['date_start']
        end = start + timedelta(hours=1)
        string = f'{start.strftime("%Hh%M"):<8s}{end.strftime("%Hh%M"):>8s}'
        string += '\n\r'
        # Line 2: rain levels
        string += '  '
        for rain_level in self.weather_data.rain['rain']:
            string += chr(self.rain_level_map[rain_level])
        self.__show_string(string)

    def __show_netatmo_temperatures_and_co2(self):
        if not self.weather_data.temperatures_and_co2:
            self.__show_string('No temperature\n\rdata')
            return

        data = self.weather_data.temperatures_and_co2
        int_module = 'Salon'
        out_module = 'Balcon'

        # Line 1: indoor
        string = f'Int: {data[int_module]["temperature"]:>4}'\
                 f'{chr(self.custom_lcd_chars["degree"])}C'\
                 f' {data[int_module]["co2"]:>4}'
        string += '\n\r'
        # Line 2: outdoor
        string += f'Ext: {data[out_module]["temperature"]:>4}'\
                  f'{chr(self.custom_lcd_chars["degree"])}C'
        self.__show_string(string)

    def __convert_dark_sky_icon(self, icon):
        # Cf https://github.com/Detrous/darksky/blob/master/darksky/forecast.py
        # Cf https://darksky.net/dev/docs#data-point
        if icon == 'clear-day' or icon == 'clear-night':
            return 'Soleil'

        if icon == 'partly-cloudy-day' or\
           icon == 'partly-cloudy-night' or\
           icon == 'cloudy':
            return 'Nuages'

        if icon == 'rain':
            return 'Pluie'

        if icon == 'snow':
            return 'Neige'

        if icon == 'wind':
            return 'Vent'

        if icon == 'fog':
            return 'Fog'

        if icon == 'sleet':
            return 'Gresil'

        return '??'

    def __show_forecast_index(self, index):
        if not self.weather_data.forecast:
            self.__show_string('No weather data')
            return

        i = 0
        for day in self.weather_data.forecast:
            if i == index:
                # Line 1: date and summary
                string = f'{day.time.strftime("%a%d/%m")} '\
                         f'{self.__convert_dark_sky_icon(day.icon):>6}'
                string += '\n\r'
                # Line 2: min/max temperatures
                temp_min = f'{round(day.temperature_min, 1)}'\
                           f'{chr(self.custom_lcd_chars["degree"])}C'
                string += f'{temp_min:<8}'
                temp_max = f'{round(day.temperature_max, 1)}'\
                           f'{chr(self.custom_lcd_chars["degree"])}C'
                string += f'{temp_max:>8}'
                self.__show_string(string)
                break
            else:
                i = i + 1

    def __show_forecast(self):
        index = self.current_page - 2
        log.debug(index)
        self.__show_forecast_index(index)

    def show_current_page(self):
        log.debug(f'Current page: {self.current_page}')
        self.pages[self.current_page]()

    def show_next_page(self):
        self.current_page = (self.current_page + 1) % len(self.pages)
        self.show_current_page()
