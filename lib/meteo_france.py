#! /usr/bin/python3

import requests
import logging
import re
from datetime import datetime, timedelta
from dateutil import tz

# Logging configuration
logging.basicConfig()
log = logging.getLogger('rain_forecast')
log.setLevel(logging.INFO)


def convert_cookie(matchobj):
    """Convert the mfsession cookie value to an authorization bearer,
       as it is done by the meteofrance.com website.

        Parameter:
            matchobj: Cookie character.
    """
    e = matchobj.group(0)
    if e <= 'Z':
        t = 65
    else:
        t = 97
    return chr(t + (ord(e) - t + 13) % 26)


class MeteoFranceRainForecast:
    """Retrieve rain forecast for a given location.

    Parameter:
        coordinates: Coordinates [latitude, longitude] of the location.
    """

    def __init__(self, latitude, longitude):
        """RainForecast initializer."""
        self.latitude = latitude
        self.longitude = longitude
        r = requests.get('http://meteofrance.com/')
        # Retrieve and convert mfsession cookie to authorization bearer
        self.bearer = re.sub(r'[a-zA-Z]',
                             convert_cookie,
                             r.cookies['mfsession'])
        log.debug(f'Bearer: {self.bearer}')

    def get_rain_forecast(self):
        """Retrieve rain forecast for the next hour in the given location.

        Returns a tuple with:
            The forecast start date.
            An array of 12 rain levels (1 to 4), one per 5 minutes range.
        Or None on failure
        """
        if not self.latitude or not self.longitude:
            return None

        # Retrieve rain for the next hour
        try:
            r = requests.get(
                url='http://rpcache-aa.meteofrance.com/'
                    'internet2018client/2.0/nowcast/rain',
                params={
                    'lat': self.latitude,
                    'lon': self.longitude
                },
                headers={
                    'Authorization':
                    f'Bearer {self.bearer}'
                }
            )
            r.raise_for_status()
            forecast = r.json()['properties']['forecast']

            # Date should be localized to France
            date_offset = tz.gettz('Europe/Paris').utcoffset(datetime.utcnow())
            date_start = datetime.strptime(
                            forecast[0]['time'],
                            '%Y-%m-%dT%H:%M:%S.%fZ'
                        ) + date_offset
            log.debug(f'Start date: {date_start}')
            rain = []
            for data in forecast:
                rain.append(data['rain_intensity'])
            log.debug(rain)
            return {'date_start': date_start, 'rain': rain}
        except requests.exceptions.HTTPError as err:
            log.error(f'Failed to retrieve rain forecast: {err}')
            return None
